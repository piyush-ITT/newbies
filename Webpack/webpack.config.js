const path = require('path');

module.exports = {
    entry: ['./app/app.js',
            './app/view1/view1.js',
            './app/view2/view2.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'webpack.bundle.js'
    }
};
